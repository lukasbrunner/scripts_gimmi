#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2020 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: A collection of functions to read in CMIP6 data from the ETHZ next
generation archive.
"""
import os
import warnings
import numpy as np
import xarray as xr
from glob import glob
from scipy import stats
from natsort import natsorted, ns

# CMIP6_PATH = '/net/atmos/data/cmip6-ng/{varn}/{tres}/{grid}'
CMIP6_PATH = '/net/ch4/data/cmip6-Next_Generation/{varn}/{tres}/{grid}'
CMIP6_FN_PATTERN = '{varn}_{tres}_{model}_{experiment}_{variant}_{grid}.nc'

VALID_LON_UNITS = [
    'degree_east',
    'degrees_east',
]
VALID_LAT_UNITS = [
    'degree_north',
    'degrees_north',
]


def intersect_models(*args):
    """
    Return intersection of filenames available for all cases.

    Parameters
    ----------
    *args : One or more list of filenames

    Returns
    -------
    *args : tuple of dict, same length as input
        One ore more dictionaries of filenames available for all models
        (and variants) from the input lists. The dictionary keys are
        <model>_<variant>
    """

    if len(args) == 1:
        return args[0]

    filenames_dict = ()
    for filenames in args:
        models = extract_models_from_filenames(filenames)
        variants = extract_variants_from_filenames(filenames)
        filenames_dict += (
            {f'{model}_{variant}': filename
             for model, variant, filename in zip(models, variants, filenames)},)

    for filenames in filenames_dict:
        try:
            intersected_model_variants = list(
                np.intersect1d(intersected_model_variants, list(filenames.keys())))
        except NameError:
            intersected_model_variants = list(filenames.keys())

    args = ()
    for filenames in filenames_dict:
        args += ({key: filenames[key] for key in intersected_model_variants},)

    return args


def get_filenames(varn, model='*', experiment='historical', variant='*', tres='ann', grid='native'):
    """
    Return a sorted list of filenames for a given setting.

    Parameters
    ----------
    varn : str
        A valid variable name.
    model : str, optional
        A valid model name.
    experiment : str, optional
        A valid experiment name.
    variant : str, optional
        A valid experiment name.

    Returns
    -------
    filenames : ndarray of str
        A list of filenames matching the input criteria.

    Notes
    -----
    All input characters can contain a single '*' string to match all possible values.
    """
    path = CMIP6_PATH.format(varn=varn, tres=tres, grid=grid)
    fn_filter = CMIP6_FN_PATTERN.format(
        varn=varn, model=model, experiment=experiment, variant=variant, tres=tres, grid=grid)
    return np.array(natsorted(glob(os.path.join(path, fn_filter)), alg=ns.IC))


def extract_models_from_filenames(filenames):
    """
    Return the model name based on the given filenames.

    Parameters
    ----------
    filenames : str or list of str
        A list of valid CMIP6ng filenames.

    Returns
    -------
    models : str or ndarray of str based on input
    """
    if isinstance(filenames, str):
        return os.path.basename(filenames).split('_')[2]
    return np.array([os.path.basename(fn).split('_')[2] for fn in filenames])


def extract_variants_from_filenames(filenames):
    """
    Return the variant ID based on the given filenames.

    Parameters
    ----------
    filenames : str or list of str
        A list of valid CMIP6ng filenames.

    Returns
    -------
    variants : str or ndarray of str based on input
    """
    if isinstance(filenames, str):
        return os.path.basename(filenames).split('_')[4]
    return np.array([os.path.basename(fn).split('_')[4] for fn in filenames])


def cluster_by_models(filenames, return_idx=False, return_keys=True, return_filenames=False):
    """
    Return a nested list of indices separating variants of the same model.

    Parameters
    ----------
    filenames : str or list of str or dict of str
        A list of valid CMIP6ng filenames.

    Returns
    -------
    nested : list of lists
    """
    if return_keys and not isinstance(filenames, dict):
        raise ValueError('filenames has to be dict if return_keys is True')

    if isinstance(filenames, str):
        filenames = [filenames]
    elif isinstance(filenames, dict):
        model_variants = np.array([*filenames.keys()])
        models = np.array([model_variant.split('_')[0]
                           for model_variant in model_variants])
        filenames = [filenames[key] for key in model_variants]
    else:
        models = extract_models_from_filenames(filenames)

    idx_nested = []
    for model in natsorted(np.unique(models), alg=ns.IC):
        idx = np.where(models == model)[0]
        idx_nested.append(idx)
    if return_idx:
        return idx_nested
    elif return_filenames:
        return [[filenames[idx] for idx in idxs] for idxs in idx_nested]
    return [[model_variants[idx] for idx in idxs] for idxs in idx_nested]


def calculate_trend(da, time_name='time'):
    """Calculate the linear least-squares trend"""
    def trend_data(data):
        if np.any(np.isnan(data)):
            return np.nan
        return stats.linregress(np.arange(len(data)), data)[0]

    return xr.apply_ufunc(
        trend_data, da,
        input_core_dims=[[time_name]],
        vectorize=True,
        keep_attrs=False)


def get_fx_fn(filename, varn):
    fn = os.path.basename(filename)
    varn_old = fn.split('_')[0]
    tres = fn.split('_')[1]
    filename = filename.replace(varn_old, varn).replace(tres, 'fx')
    if not os.path.isfile(filename):
        return None
    return filename


def weighted_mean(data, weights):
    return np.average(data, weights=weights)


def get_longitude_name(ds):
    """Get the name of the longitude dimension by CF unit"""
    lonn = []
    for dimn in ds.dims:
        if ('units' in ds[dimn].attrs and
            ds[dimn].attrs['units'] in VALID_LON_UNITS):
            lonn.append(dimn)
    if len(lonn) == 1:
        return lonn[0]
    elif len(lonn) > 1:
        errmsg = 'More than one longitude coordinate found by unit.'
    else:
        errmsg = 'Longitude could not be identified by unit.'
    raise ValueError(errmsg)


def get_latitude_name(ds):
    """Get the name of the latitude dimension by CF unit"""
    latn = []
    for dimn in ds.dims:
        if ('units' in ds[dimn].attrs and
            ds[dimn].attrs['units'] in VALID_LAT_UNITS):
            latn.append(dimn)
    if len(latn) == 1:
        return latn[0]
    elif len(latn) > 1:
        errmsg = 'More than one latitude coordinate found by unit.'
    else:
        errmsg = 'Latitude could not be identified by unit.'
    raise ValueError(errmsg)


def _area_weighted_mean_data(data, lat, lon):
    """Calculates an area-weighted average of data depending on latitude [and
    longitude] and handles missing values correctly.

    Parameters:
    - data (np.array): Data to be averaged, shape has to be (..., lat[, lon]).
    - lat (np.array): Array giving the latitude values.
    - lon (np.array): Array giving the longitude values (only used for
      consistency checks). lon can be None in which case data is considered
      as not containing the longitude dimension (hence latitude has to be the
      last dimension).

    Returns:
    Array of shape (...,) or float"""
    # input testing & pre-processing
    was_masked = False
    if isinstance(data, np.ma.core.MaskedArray):
        was_masked = True
    else:  # in case it a Python list
        data = np.array(data)
    lat = np.array(lat)
    assert len(lat.shape) == 1, 'lat has to be a 1D array'
    assert ((-90 <= lat) & (lat <= 90)).all(), 'lat has to be in [-90, 90]!'
    if lon is None:
        assert data.shape[-1] == len(lat), 'Axis -1 of data has to match lat!'
        data, lon = data.reshape(data.shape + (1,)), np.array([])
    else:
        lon = np.array(lon)
        assert len(lon.shape) == 1, 'lon has to be a 1D array'
        assert data.shape[-1] == len(lon), 'Axis -1 of data has to match lon!'
        assert data.shape[-2] == len(lat), 'Axis -2 of data has to match lat!'
        errmsg = 'lon has to be in [-180, 180] or [0, 360]!'
        assert (((-180 <= lon) & (lon <= 180)).all() or
                ((0 <= lon) & (lon <= 360)).all()), errmsg

    # create latitude weights and tile them to all longitudes, then flatten
    w_lat = np.cos(np.radians(lat))
    weights = np.tile(w_lat, (data.shape[-1], 1)).swapaxes(0, 1).ravel()
    # flatten lat-lon dimensions, mask missing values, average
    data_flat = np.ma.masked_invalid(data.reshape(data.shape[:-2] + (-1,)))
    mean = np.ma.average(data_flat, axis=-1, weights=weights)

    # NOTE: if data is a single value and not invalid it will be a normal
    # float at this point and no longer masked!
    if isinstance(mean, (float, int)):
        return mean
    elif was_masked:  # if input was masked array also return a masked array
        return mean
    return mean.filled(fill_value=np.nan)


def area_weighted_mean(
        ds, latn=None, lonn=None, keep_attrs=True, suppress_warning=False):
    """xarray version of utils_python.physics.area_weighed_mean

    Parameters
    ----------
    ds : {xarray.Dataset, xarray.DataArray}
        Has to contain at least latitude and longitude dimensions
    lonn : string, optional
    latn : string, optional
    keep_attrs : bool, optional
        Whether to keep the global attributes
    suppress_warning : bool, optional
        Suppress warnings about nan-only instances

    Returns
    -------
    mean : same type as input with (lat, lon) dimensions removed
    """
    if suppress_warning:
        warnings.simplefilter('ignore')
    if latn is None:
        latn = get_latitude_name(ds)
    if lonn is None:
        lonn = get_longitude_name(ds)

    was_da = isinstance(ds, xr.core.dataarray.DataArray)
    if was_da:
        ds = ds.to_dataset(name='data')

    ds_mean = ds.mean((latn, lonn), keep_attrs=keep_attrs)  # create this just to fill
    for varn in set(ds.variables).difference(ds.dims):
        if latn in ds[varn].dims and lonn in ds[varn].dims:
            var = ds[varn].data
            axis_lat = ds[varn].dims.index(latn)
            axis_lon = ds[varn].dims.index(lonn)
            var = np.moveaxis(var, (axis_lat, axis_lon), (-2, -1))
            mean = _area_weighted_mean_data(var, ds[latn].data, ds[lonn].data)
        elif latn in ds[varn].dims:
            var = ds[varn].data
            axis_lat = ds[varn].dims.index(latn)
            var = np.moveaxis(var, axis_lat, -1)
            mean = _area_weighted_mean_data(var, ds[latn].data)
        elif lonn in ds[varn].dims:
            mean = ds[varn].mean(lonn).data
        else:
            continue

        ds_mean[varn].data = mean

    warnings.resetwarnings()
    if was_da:
        return ds_mean['data']
    return ds_mean
