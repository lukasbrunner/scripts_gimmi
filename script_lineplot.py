#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
(c) 2020 under a MIT License (https://mit-license.org)

Authors:
- Lukas Brunner || lukas.brunner@env.ethz.ch

Abstract: A simple script for plotting CMIP6 timeseries
"""
import os
import xarray as xr
import matplotlib.pyplot as plt
# import seaborn as sns

import functions as func

PLOTPATH = 'figures/'


def main():
    # define input
    varn = 'tas'
    experiment = 'ssp126'
    period = ('1950', '2100')
    base_period = ('1980', '2014')

    # get all available files
    filenames_hist = func.get_filenames(varn)
    filenames_ssp126 = func.get_filenames(varn, experiment=experiment)

    # get only files available for both
    filenames_hist, filenames_ssp126 = func.intersect_models(filenames_hist, filenames_ssp126)

    # cluster variants of the same model together
    keys_nested = func.cluster_by_models(filenames_hist)

    # define axis and colors
    fig, ax = plt.subplots(figsize=(9, 6))
    # colors = sns.color_palette(n_colors=len(keys_nested))
    xx = 0

    for keys in keys_nested:
        model = keys[0].split('_')[0]
        ds_list = []

        # # NOTE: use only models with more than XX variants
        # if len(keys) < 3:
        #     continue

        # for key in keys:
        for key in keys[:10]:  # NOTE: plot maximum of 10 variants
            # actually open the files
            ds_hist = xr.open_dataset(filenames_hist[key], use_cftime=True)
            ds_ssp126 = xr.open_dataset(filenames_ssp126[key], use_cftime=True)

            # merge historical and forces simulations
            ds = xr.concat([ds_hist, ds_ssp126], dim='time')

            # select a given time period
            ds = ds.sel(time=slice(*period))

            # take the global mean
            ds = func.area_weighted_mean(ds)

            ds_list.append(ds)

        # merge all variants into one dataset
        ds = xr.concat(ds_list, dim='variant')

        # select variable
        da = ds[varn]

        if base_period is not None:
            da -= da.sel(time=slice(*base_period)).mean('time')

        # plot thick line for variant mean
        [line] = da.mean('variant').plot.line(
            x='time', lw=2, label=f'{model} ({len(keys)})', zorder=100,
            #color=colors[xx]
        )

        # if more than one variant plot thin line for each
        if len(keys) > 1:
            da.plot.line(
                x='time', color=line.get_color(), lw=.3, zorder=10, add_legend=False)

        xx += 1

    # --- plot "observations" ---
    era5 = xr.open_dataset(
        '/net/h2o/climphys/lukbrunn/Data/InputData/ERA5/g025/tas_mon_ERA5_g025.nc',
        use_cftime=True)['tas']
    era5 = era5.sel(time=slice(*period))

    # calculate annual means from monthly means
    era5 = era5.resample(time='1A').mean()

    era5 = func.area_weighted_mean(era5)

    if base_period is not None:
        era5 -= era5.sel(time=slice(*base_period)).mean('time')

    era5.plot.line(x='time', color='k', lw=2, label='ERA5', zorder=200)
    # ---

    # define some plot settings
    ax.set_xlim(da['time'].data[0], da['time'].data[-1])
    ax.set_xlabel('Year')
    if base_period is None:
        ax.set_ylabel('Temperature (K)')
    else:
        ax.set_ylabel(f'Temperature anomalie relative to {"-".join(base_period)} (K)')
    ax.legend(fontsize='xx-small', ncol=3)
    ax.set_title(f'Temperature time-series historical and {experiment.upper()}')
    plt.savefig(os.path.join(PLOTPATH, f'timeseries_{varn}_{experiment}_1.png'), dpi=300)


if __name__ == '__main__':
    main()
